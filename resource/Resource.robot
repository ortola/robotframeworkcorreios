
*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${URL}        https://buscacepinter.correios.com.br/app/endereco/index.php
${BROWSER}    headlesschrome
#headlesschrome

*** Keywords ***
#### SETUP E TEARDOWN
Abrir navegador
      Open Browser    about:blank   ${BROWSER}

Fechar navegador
      Close Browser

### PASSO A PASSO
Acessar a página home do site
      Go To    https://buscacepinter.correios.com.br/app/endereco/index.php
      Title Should Be    Busca CEP

Digitar o CEP "${CEP}"
      Input Text    name=endereco    ${CEP}
      Capture Page Screenshot  cepdigitado.png


Selecionar tipo de CEP "${TIPOCEP}"
      Select From List By Value    name=tipoCEP    ${TIPOCEP}
      Capture Page Screenshot  tipocep.png

Clicar no botão buscar
      Click Element    name=btn_pesquisar

Validar retorno do logradouro igual a "${LOGRADOURO}"
  Wait Until Element Is Visible       css=#resultado-DNEC > tbody > tr > td:nth-child(1)
  Wait Until Page Contains Element    css=#resultado-DNEC > tbody > tr > td:nth-child(1)
  Page Should Contain                 text=${LOGRADOURO}
  Capture Page Screenshot             logradouroretornato.png

Validar mensagem de retorno igual a "${MSGRETORNOINEXISTENTE}"
  Wait Until Element Is Visible     css=#mensagem-resultado
  Page Should Contain               text=${MSGRETORNOINEXISTENTE}
  Capture Page Screenshot           mensagemretornoinexistente.png



#Conferir se o produto "${PRODUTO}" foi listado no site
    #  Wait Until Element Is Visible    css=#center_column > h1
    #  Title Should Be                  Search - My Store
    #  Page Should Contain Image        xpath=//*[@id="center_column"]//*[@src='http://automationpractice.com/img/p/7/7-home_default.jpg']
    #  Page Should Contain Link         xpath=//*[@id="center_column"]//a[@class="product-name"][contains(text(),"${PRODUTO}")]
