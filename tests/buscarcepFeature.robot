*** Settings ***
Resource        ../resource/Resource.robot
Test Setup      Abrir navegador
Test Teardown   Fechar navegador

*** Test Case ***
Caso de Teste 01: Pesquisar por CEP válido
      Acessar a página home do site
      Digitar o CEP "21721600"
      Selecionar tipo de CEP "LOG"
      Clicar no botão buscar
      Validar retorno do logradouro igual a "Travessa Marechal Marciano"

Caso de Teste 02: Pesquisar por CEP inválido
      Acessar a página home do site
      Digitar o CEP "21121211"
      Selecionar tipo de CEP "LOG"
      Clicar no botão buscar
      Validar mensagem de retorno igual a "Não há dados a serem exibidos"
      #Conferir se o produto "Blouse" foi listado no site
